Those dotfiles are heavily inspired from i3-starterpack but with my own setting and little modification

Install I3:`sudo apt-get install i3`

Install other components: ` sudo apt-get install compton hsetroot rxvt-unicode xsel rofi fonts-noto fonts-mplus xsettingsd lxappearance scrot viewnior feh vim`

To make urxvt usable add this line to your shell script(.bashrc): `export TERM=xterm-256color`

You will need to Enter the full path for Feh in the i3 config files

For Polybar you need the `build-essentials`
other depedencies: `sudo apt install cmake cmake-data libcairo2-dev libxcb1-dev libxcb-ewmh-dev libxcb-icccm4-dev libxcb-image0-dev libxcb-randr0-dev libxcb-util0-dev libxcb-xkb-dev pkg-config python-xcbgen xcb-proto libxcb-xrm-dev i3-wm libasound2-dev libmpdclient-dev libiw-dev libcurl4-openssl-dev libpulse-dev libxcb-composite0-dev xcb libxcb-ewmh2`

Final result with one of my Wallpapers

![Screenshot](Wallpapers.png)